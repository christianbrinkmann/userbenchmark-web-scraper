import json, time

import stringparser
import downloader


id = 2000000

start = time.time()

"""while id <= 100:
    print(id)
    content = downloader.downloadFile(id)
    if content is not None:
        data = stringparser.parse(content)
    id+=1"""

data = {}

content = downloader.downloadFile(id)
dl = time.time()
if content is not None:
    print("Call")
    data = stringparser.parse(content)

print("Download: "+str(dl - start))
print("Parse: "+str(time.time() - dl))

print(json.dumps(data, indent=4))
