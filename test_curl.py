from urllib.request import Request, urlopen
import urllib3
import urllib.request
import socket

import src.header as header


#https://amiunique.org/fp
user_agent = header.get_user_agent()
url = 'https://userbenchmark.com'
values = header.get_values()

headers = {'User-Agent': user_agent}
headers['ACCEPT'] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
headers['REFERER'] = "https://www.google.com/"
headers['Accept-Language'] = "en-US"


data = urllib.parse.urlencode(values).encode("utf-8")
req = urllib.request.Request(url, data)
for k in headers:
    print(k+":")
    print("    "+str(headers[k]))
    req.add_header(k, headers[k])
for r in req.headers:
    print(r+": "+req.headers[r])
response = urllib.request.urlopen(req)
rh = response.headers
print("RH:")
if rh is not None:
    for h in rh:
        print(h+": "+rh[h])
else:
    print("Response Header leer")
the_page = response.read()
print(the_page)