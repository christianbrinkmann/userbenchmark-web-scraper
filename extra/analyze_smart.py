import sqlite3, json, os


def ana(cur, data):
    for d in cur:
        if len(d[0]) > 0:
            j = json.loads(d[0])
            for entry in j.values():
                id = entry["ID"]
                if id in data:
                    if entry["Attribute"] not in data[id]:
                        data[id].append(entry["Attribute"])
                else:
                    data[id] = []
                    data[id].append(entry["Attribute"])
    return data

data = {}
conn = sqlite3.connect("userbenchmark.db")
cur = conn.cursor()

print("SELECT FROM hdd")
cur.execute("SELECT SMART FROM hdd")
data = ana(cur, data)
print("SELECT FROM ssd")
cur.execute("SELECT SMART FROM ssd")
data = ana(cur, data)

conn.close()

#print("Writing JSON")

#print(data)

#with open(os.path.join("errors", 'SMART.json'), 'w') as outfile:
    #json.dump(data, outfile)
    
ks = list(data.keys())
ks.sort()

print("Write list")

with open(os.path.join("errors", 'SMART.keys'), 'w') as outfile:
    outfile.write(str(ks))

print("All done.")