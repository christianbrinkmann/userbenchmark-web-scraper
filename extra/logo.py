# importing image object from PIL 
import math 
from PIL import Image, ImageDraw 

import matplotlib.pyplot as plt
  
w, h = 1024, 1024
shape = [(40, 40), (w - 10, h - 10)] 
  

def draw_arc(image, xy, start, finish, fill="black", width=1):
    length = abs(xy[0][0]-xy[1][0]) / 2
    center = (xy[0][0]+0.5*(xy[1][0]-xy[0][0]), xy[0][1]+0.5*(xy[1][1]-xy[0][1]))
    rads_start = start / 180 * math.pi
    direction_start = (math.cos(rads_start), math.sin(rads_start))
    rads_finish = finish / 180 * math.pi
    direction_finish = (math.cos(rads_finish), math.sin(rads_finish))
    sp = (center[0]+direction_start[0]*(length),center[1]+direction_start[1]*(length))
    fp = (center[0]+direction_finish[0]*(length),center[1]+direction_finish[1]*(length))
    dis = 5 + int(length * 5*abs(rads_finish-rads_start))
    for i in range(dis):
        angle = start + i * (finish-start) / (dis)
        rads = angle / 180 * math.pi
        direction = (math.cos(rads), math.sin(rads))
        image.line([(center[0]+direction[0]*(length),center[1]+direction[1]*(length)),(center[0]+direction[0]*(length - width),center[1]+direction[1]*(length - width))], fill=fill, width=2)



# creating new Image object 
img = Image.new("RGB", (w, h)) 
  
# create rectangle image 
img1 = ImageDraw.Draw(img)
img1.rectangle([(0,0),(w,h)], fill="white", outline = None) 
b = 64
#img1.arc([(0,0),(w,h)], 0, 360, fill="black", width=8)
draw_arc(img1,[(0,0),(w,h)], 0, 360, fill="black", width=16)
#img1.arc([(b, b), (w-b, h-b)] , 157.5, 22.5, fill="red", width=15)

#img1.arc([(8,8),(w-8,h-8)], 0, 360, fill="#FDA217", width=8)
draw_arc(img1,[(8,8),(w-8,h-8)], 0, 360, fill="#FDA217", width=16)
#FDA217


intensity = 196
parts = 1000
variance = 200

_start = 150
_finish = _start + 180 + 60
_step = (_finish - _start) / parts
vs = []
for i in range(parts):
    color_boost = (1 ) * math.exp(-(i-parts/2)**2 / (2*variance**2) ) + 1
    #color_boost = (max(0,100 - 0.001*(i-parts/2)**2)/100 + 1)
    vs.append(color_boost-1)
    #color_boost = 1
    #print("Boost: "+str(color_boost))
    cl = (int(color_boost*(intensity*(parts-i)/parts)), int(color_boost*intensity*i/parts), 0)
    #print("Color: "+str(cl))
    c = ('#%02x%02x%02x' % cl)
    #img1.arc([(b, b), (w-b, h-b)] , (_start+i*_step) % 360, (_start+(i+1)*_step) % 360, fill=c, width=15)
    #print("Start: "+str((_start+i*_step) % 360)+"    Finish: "+str((_start+(i+1)*_step) % 360))
    draw_arc(img1,[(b, b), (w-b, h-b)] , (_start+i*_step), (_start+(i+1)*_step), fill=c, width=30)

#pos = (w/2 + ,)
#img1.line([(,),(,)])


#img1.arc([(w/4,h/4),(w*3/4,h*3/4)],180,360,fill="black", width=1)
draw_arc(img1,[(w*15/32,h*15/32),(w*17/32,h*17/32)],0,360,fill="black", width=w/64)

def draw_line(image, w, h, padding, length, angle, width):
    center = (w/2, h/2)
    rads = angle / 180 * math.pi
    direction = (math.cos(rads), math.sin(rads))
    image.line([(center[0]+direction[0]*(w/2 - padding),center[1]+direction[1]*(w/2 - padding)),(center[0]+direction[0]*(w/2 - padding - length),center[1]+direction[1]*(w/2 - padding - length))], fill="black", width=width)

#def draw_zeiger(image, w, h, angle, length, depth, width, fill="black"):
#    for j in range(-width, width):
        


for i in range(5):
    draw_line(img1, w, h, b-2, 50, _start+(_finish-_start)/4*i, 5)

for i in range(4*6):
    draw_line(img1, w, h, b, 30, _start+(_finish-_start)/24*i, 2)

#img1.rectangle(shape, fill ="#ffff33", outline ="red") 
img.save("C:\\Users\\Christian\\Downloads\\test.png")
img.show()

plt.plot(vs)
plt.show()
