import re, json

f = open("countries_tor_tmp.txt", "r")
country_codes = []
for x in f:
    m = re.search(r"<td>\{([a-z]{2})\}</td>", x)
    if m is not None:
        print(m.group(1))
        country_codes.append(m.group(1))

with open("countries_tor.txt", "w") as fp:   #Pickling
    for l in country_codes:
        fp.write(l+"\n")