hdd, ssd: (int) smart_id [id]

SMART: (int) [id] autoincrement primary, (int) hdd_id, (int) ssd_id, (int) start_id, (int) stop_id
(((include start_id, exclude stop_id)))

SMART_ENTRY: (int) id autoincrement primary, (int) smart_id, (int) code (((from hexadecimal to int (8 bit)))), (int) now, (int) worst, (int) value

SMART_ATTRIBUTE: (int) primary code (((from hexadecimal to int (8 bit)))), (TEXT) name