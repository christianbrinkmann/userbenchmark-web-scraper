import time, sys
from threading import Lock

import config


def print_config():
    module = globals().get("config", None)
    book = {}
    if module:
        book = {key: value for key, value in module.__dict__.items() if not (key.startswith('__') or key.startswith('_'))}
    for b in book:
        print(str(b)+": "+str(book[b]), flush=True)
    print("----------", flush=True)

print("Starting server_main.py", flush=True)
args = sys.argv
if args is not None and len(args) > 1:
    for arg in args[1:]:
        if arg.lower() == "--no-input":
            config.USE_INPUT = False
            print("Deactive user input", flush=True)
        if arg.startswith("--database="):
            db = arg[11:]
            print("Use database \""+db+"\"")
            config.DATABASE_FILE = db

print("Config: ", flush=True)
print_config()

import src.server as server
import src.server_logger as server_logger
import src.amount as amount
import src.database as database
import src.ids as ids

print("Try to get amount...", flush=True)

a = amount.get_amount()

if a == 0:
    print("Could not get amount...", flush=True)
    exit()

ids.init_ids(a)
print("IDs initialized...", flush=True)
count = database.load_ids(ids.ids)
print("ids loaded: "+str(count), flush=True)

server_logger.run_server_logger()

threads_mutex = Lock()
threads = []
server.run_server(threads, threads_mutex)