import time, os, subprocess, sys

import src.client as client
import src.country_code as country_code
import src.proxy_list as proxy_list
import config

def _start_client():
    return client.run_client(config.CLIENT_HOST_ADDRESS, config.CLIENT_HOST_PORT)

thread = None

args = sys.argv
if args is not None and len(args) > 1:
    for arg in args[1:]:
        if arg.lower() == "help" or arg.lower() == "-help" or arg.lower() == "--help" or arg.lower() == "?":
            print("Help:")
            print("  "+args[0]+" <argument>")
            print("    --host=<host>")
            print("    --no-input")
            print("    --proxy-list=<file>")
            print("    --help")
            exit()
        elif arg.lower().startswith("--host="):
            config.CLIENT_HOST_ADDRESS = arg[7:]
            print("Use host '"+arg[7:]+"'")
        elif arg.lower() == "--no-input":
            config.USE_INPUT = False
            print("Deactive user input", flush=True)
        elif arg.lower().startswith("--proxy-list="):
            config.PROXY_LIST_FILE = arg[13:]
            print("Use proxy list file '"+arg[13:]+"'")
        

print("Host: "+str(config.CLIENT_HOST_ADDRESS), flush=True)

print("Load country_code...", flush=True)
country_code.load_country_code_from_file()

if config.PROXY_LIST_FILE is not None:
    if not os.path.exists(config.PROXY_LIST_FILE):
        print("Proxy list file '"+config.PROXY_LIST_FILE+"' does not exist!", flush=True)
        exit()
    proxy_list.load_file(config.PROXY_LIST_FILE)
    print("Proxy list loaded.", flush=True)
else:
    print("Use Tor", flush=True)

while True:
    cmd = ""
    thread = _start_client()
    while thread.keep_running:
        if config.USE_INPUT:
            cmd = input()
            if cmd == "stop" or cmd == "end":
                print("Stopping...", flush=True)
                thread._run = False
        time.sleep(1)
        if thread._run == False:
            print("Detected that client thread is no longer running...", flush=True)
            thread = _start_client()
            print("Restarted client thread.", flush=True)
    time.sleep(1)
print("Stopped.", flush=True)
