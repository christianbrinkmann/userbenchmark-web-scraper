echo "Starting tor..."
sleep 3s
sudo -u tor tor &
echo "Host IP: "
curl -s ifconfig.me
echo ""
echo "Tor IP: "
curl -s -x socks5://127.0.0.1:9050 ifconfig.me
echo ""
echo "Starting server in ... 10"
for i in 9 8 7 6 5 4 3 2 1
do
   echo "                   ...  $i"
   sleep 1s
done
/usr/bin/python3 server_main.py --no-input --database=database/userbenchmark.db
