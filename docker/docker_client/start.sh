echo "Starting tor..."
sudo -u tor tor &
echo "Host IP: "
curl -s ifconfig.me && echo
echo "Tor IP: "
curl -s -x socks5://127.0.0.1:9050 ifconfig.me && echo
echo "Starting client in ... 10"
for i in 9 8 7 6 5 4 3 2 1
do
   echo "                   ...  $i"
   sleep 1s
done
/usr/bin/python3 client_main.py --host=userbenchmark-scraper-server --no-input
