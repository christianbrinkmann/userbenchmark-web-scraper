#!/bin/bash

country_file="countries_tor.txt"

if [ -z $1 ]; then
  echo "Missing argument."
  echo "$0 <build|create|start|stop|remove|list|help>"
else
  if [ -r $country_file ]; then
    if [ $1 == "build" ]; then
      echo "Build"
      while read p; do
        docker build --build-arg COUNTRY_CODE=$p -t userbenchmark-scraper-$p:latest .
      done <$country_file
    elif [ $1 == "create" ]; then
      echo "Create"
      while read p; do
        docker create -v /errors:/userbenchmark-web-scraper/errors --name=userbenchmark-scraper-$p --network userbenchmark-net userbenchmark-scraper-$p
      done <$country_file
    elif [ $1 == "start" ]; then
      echo "Start"
      while read p; do
        docker start userbenchmark-scraper-$p
      done <$country_file
    elif [ $1 == "stop" ]; then
      echo "Stop"
      while read p; do
        docker stop userbenchmark-scraper-$p
      done <$country_file
    elif [ $1 == "remove" ]; then
      echo "Remove"
      while read p; do
        docker container rm userbenchmark-scraper-$p
        docker image rm userbenchmark-scraper-$p
      done <$country_file
    elif [ $1 == "help" ]; then
      echo "Help"
    elif [ $1 == "list" ]; then
      while read p; do
        echo $p
      done <$country_file
    else
      echo "Unknown argument \"$1\"."
      echo "$0 <build|create|start|stop|remove|help>"
    fi
  else
    echo "\"$country_file\" does not exist or can not be read!"
  fi
fi
if [ "a" == "b" ]; then
  echo "A"
  docker build --build-arg COUNTRY_CODE=de -t userbenchmark-scraper:latest .
  docker create -v /errors:/userbenchmark-web-scraper/errors --name=userbenchmark-scraper --network userbenchmark-net userbenchmark-scraper
  docker start userbenchmark-scraper
  #ai
  docker stop userbenchmark-scraper
  docker container rm userbenchmark-scraper
  docker image rm userbenchmark-scraper
fi
