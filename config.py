
#General config
RECEIVE_TIMEOUT = 120
USE_INPUT = True
SERVER_HOST_ADDRESS = "0.0.0.0"
SERVER_HOST_PORT = 4488
PROXY_LIST_FILE = None #if "None" use tor, otherwise use proxies from list

#Client config
CLIENT_HOST_ADDRESS = "127.0.0.1"
CLIENT_HOST_PORT = 4488

#Server config
DATABASE_FILE = "userbenchmark.db" #database file
WEB_DIR = "web" #web directory
WEB_REFRESH_WAIT = 30 #amount of seconds to wait before refreshing the web page
ACTIVE_THRESHHOLD = 300 #amount of seconds after which a client is counted as inactive
MIN_TIME_BETWEEN_IP_USED = 60*60