import time, os
from datetime import datetime
from threading import Lock, Thread

import config
from . import ids, amount

mutex = Lock()
thread = None

logs = {} #key: client-id, value: dict
                        # key: timestampt
                        # value: ip as int

def log_download(client_id, ip):
    mutex.acquire()
    if not client_id in logs:
        logs[client_id] = {}
    logs[client_id][int(time.time())] = ip
    mutex.release()

def _write_to_file(file, content):
    f = open(file, "w")
    f.write(content)
    f.close()

def time_delta_str(time_delta): #in seconds
    if time_delta < 0:
        return "!Negative time ("+str(time_delta)+")!"
    text_parts = []
    time_units = [12*30*24*60*60,30*24*60*60,24*60*60,60*60,60,1]
    time_unit_names = ["year", "month", "day", "hour", "minute", "second"]
    v = int(time_delta)
    r = time_delta - v
    for i in range(len(time_units)):
        c = v // time_units[i]
        v = v % time_units[i]
        if c > 0:
            text_parts.append(str(c)+" "+time_unit_names[i])
        if c > 1:
            text_parts[-1] = text_parts[-1]+"s"
    return " ".join(text_parts)
    
def _int_to_ip(ipnum):
    o1 = int(ipnum / 16777216) % 256
    o2 = int(ipnum / 65536) % 256
    o3 = int(ipnum / 256) % 256
    o4 = int(ipnum) % 256
    return '%(o1)s.%(o2)s.%(o3)s.%(o4)s' % locals()

header = "<?php ?><!DOCTYPE HTML><html><head><title>Userbenchmark-Web-Scraper Status Page</title>"
header += "<meta http-equiv=\"refresh\" content=\""+str(config.WEB_REFRESH_WAIT)+"\">"
header += "<style>table, th, td {border: 1px solid black; border-collapse: collapse;}</style></head><body>"
footer = "</body></html>"

class LoggingThread(Thread):

    def __init__(self):
        Thread.__init__(self)
        print("Logging thread started...", flush=True)

    def run(self):
        running = True
        while running:
            start_time = time.time()
            text = ""
            text += header
            text += "Total Progress: "+("{:5.2f}".format(ids.id_amount*100.0/amount.amount))+"%, "+str(ids.id_amount)+" / "+str(amount.amount) + " Benchmarks downloaded <br/>"
            text += "Last update: "+datetime.now().strftime("%a %d.%m.%Y, %H:%M:%S %Z")+" <br/>"
            mutex.acquire()
            try:
                text += "Total unique clients: "+str(len(logs)) + "<br/>"
                unique_clients = []
                active_clients = []
                current_time = int(time.time())
                for log in logs:
                    unique_clients.append(log)
                    for t in logs[log]:
                        if t + config.ACTIVE_THRESHHOLD >= current_time:
                            active_clients.append(log)
                            break
                text += "   "+ ",".join(unique_clients) + "<br/>"
                text += "Total active clients: "+str(len(active_clients)) + " <br/>"
                text += "   "+ ",".join(active_clients) + "<br/>"

                text += "<br/><br/><h3>Downloads per minute:</h3>"
                text += "<table><tr><th>Client-ID</th><th>Last 5 Minutes</th><th>Last Hour</th><th>Last Day</th></tr>"
                sizes = [5*60,60*60,24*60*60]
                amounts = {}
                totals = []
                for log in logs:
                    amounts[log] = []
                    for t in logs[log]:
                        for i in range(len(sizes)):
                            s = sizes[i]
                            if t + s >= current_time:
                                while i >= len(amounts[log]):
                                    amounts[log].append(0)
                                amounts[log][i] += 1
                    text += "<tr><td>"+log+"</td>"
                    for i in range(len(sizes)):
                        a = amounts[log][i]
                        text += "<td>"+("{:5.2f}".format(a*60/sizes[i]))+"</td>"
                        while i >= len(totals):
                            totals.append(0)
                        totals[i] += a
                    text += "</tr>"
                text += "<tr><td>Total</td>"
                for i in range(len(totals)):
                    t = totals[i]
                    text += "<td>"+("{:5.2f}".format(t*60/sizes[i]))+"</td>"
                text += "</tr></table>"
                active_rate = 0
                for log in logs:
                    text += "<br/><br/>"
                    text += "<h3>"+log+":</h3>"
                    ips_used = []
                    unique_ips = []
                    for t in logs[log]:
                        ips_used.append(_int_to_ip(logs[log][t]))
                        if not logs[log][t] in unique_ips:
                            unique_ips.append(logs[log][t])
                    if log in active_clients:
                        first_download = None
                        for t in logs[log]:
                            if first_download is None or first_download > t:
                                first_download = t
                        if first_download is None:
                            text += "Critical failure! client is active yet no download registred?!?"
                        else:
                            text += "Active since "+time_delta_str(current_time-first_download)
                            active_rate += len(logs[log])*60/(current_time-first_download)
                    else:
                        last_download = None
                        for t in logs[log]:
                            if last_download is None or last_download < t:
                                last_download = t
                        if last_download is None:
                            text += "Critical failure! client was once active yet no download was registred?!?"
                        else:
                            text += "Inactive since "+time_delta_str(current_time-last_download)
                    text += "<br/>"
                    text += "Downloads: "+str(len(logs[log]))+"<br/>"
                    text += "Unique ips: "+str(len(unique_ips))
                text += "<br/><br/>"
                text += "Total rate: "+"{:5.2f}".format(active_rate)+" downloads per minute<br/>"
                if active_rate != 0:
                    text += "Time to finish: "+time_delta_str(60*(amount.amount-ids.id_amount)/active_rate)
                else:
                    text += "Time to finish: infinite"
            finally:
                mutex.release()

            stop_timer = time.time()
            text += "<br/>Time used to generate this page: "+str(int(1000*(stop_timer-start_time)))+"ms"

            text += footer

            print("write index.php...")
            _write_to_file(os.path.join(config.WEB_DIR, "index.php"), text)
            print("webpage refreshed...", flush=True)
            time.sleep(config.WEB_REFRESH_WAIT)

def run_server_logger():
    global thread
    thread = LoggingThread()
    thread.start()