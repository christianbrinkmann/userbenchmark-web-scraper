import sqlite3, json, time, math, random, traceback, os
from threading import Thread, Lock

from . import ids
import config

def load_ids(list):
    return thread._load_ids(list)

def store_data(data):
    return thread._add_queue(data)


def _sonder_log(text, id):
    print("["+str(id)+"] "+str(text), flush=True)


def _update_min_max_val(current, min_v, max_v):
    v = min_v
    if min_v is None or current < min_v:
        v = current
    w = max_v
    if max_v is None or current + 1 > max_v:
        w = current + 1
    return (v, w)

def _next_group_run_id():
    ids.group_id_max += 1
    return ids.group_id_max

def _get_group_id(other_runs):
    if other_runs is None or len(other_runs) == 0:
        return _next_group_run_id()
    conn = sqlite3.connect(config.DATABASE_FILE)
    cur = conn.cursor()
    cur.execute("SELECT userrun_group FROM userrun where id = "+str(other_runs[0]))
    res = None
    for d in cur:
        if d is not None and d[0] is not None:
            res = int(d[0])
    if res is None:
        return _next_group_run_id()
    return res

class Database(Thread):

    conn = None

    data_queue = []
    queue_mutex = Lock()

    sleep_timer = 0.0

    def _add_queue(self, data):
        self.queue_mutex.acquire()
        self.data_queue.append(data)
        value = self.sleep_timer + 0.0
        self.queue_mutex.release()
        return value

    def _get_queue_size(self):
        self.queue_mutex.acquire()
        length = len(self.data_queue)
        self.sleep_timer = math.sqrt(length) / 100.0
        self.queue_mutex.release()
        return length

    def _get_next(self):
        data = None
        self.queue_mutex.acquire()
        if len(self.data_queue) > 0:
            data = self.data_queue.pop(0)
        self.queue_mutex.release()
        return data

    def _load_ids(self, list):
        print("Open \""+config.DATABASE_FILE+"\"", flush=True)
        conn = sqlite3.connect(config.DATABASE_FILE)
        cur = conn.cursor()
        cur.execute("SELECT id FROM userrun")
        count = 0
        for d in cur:
            count += 1
            ids._add_id(int(d[0]))
        
        print("Loaded ids, now loading group ids...", flush=True)

        cur = conn.cursor()
        cur.execute("SELECT MAX(userrun_group) FROM userrun")
        for d in cur:
            if d is not None and d[0] is not None:
                ids.group_id_max = int(d[0])

        conn.close()

        return count

    def run(self):
        print("open database >"+config.DATABASE_FILE+"<", flush=True)
        self.conn = sqlite3.connect(config.DATABASE_FILE)
        print("work with database", flush=True)
        data = None
        changed = False
        lasttime = 0
        while True:
            data = self._get_next()
            if data is None:
                time.sleep(1)
                if changed == True:
                    print("Commit", flush=True)
                    print("Queue empty", flush=True)
                    self.conn.commit()
                changed = False
            else:
                self._store_data(data)
                queue_length = self._get_queue_size()
                if time.time()-lasttime > 10:
                    print("Queue length: "+str(queue_length), flush=True)
                    lasttime = time.time()
                changed = True
                if random.randint(0,queue_length+1) < 100:
                    print("Commit", flush=True)
                    self.conn.commit()
                    changed = False
            

    def _store_data(self, data):
        print("  storing "+str(data["id"]), flush=True)
        try:
            #background_cpu, os, system, country, bios_date, memory_free, memory
            id = _insert_userrun(data["id"], data["Run Date"], data["Uptime"], data["Background CPU"], data["OS"], data["System"],
                data["Motherboard"], data["Motherboard Vendor"], data["Country"], data["User"], data["BIOS Date"], data["Memory free"], data["Memory"],
                data["Display Width"], data["Display Height"], data["Display Colors"], _get_group_id(data["Other Runs"]))
            update_list = []
            gpu_id_start = None
            gpu_id_end = None
            ssd_id_start = None
            ssd_id_end = None
            hdd_id_start = None
            hdd_id_end = None
            usb_id_start = None
            usb_id_end = None
            for key in data:
                if key.startswith("cpu"):
                    number = int(key[4:])
                    dic = data[key]
                    if number <= 1:
                        #print(json.dumps(dic, indent=4))
                        scores = dic["n-core score"]
                        update_list.append(("cpu_id", _insert_cpu(id, dic["name"], dic["Manufacturer"], dic["cpus"], dic["cores"], dic["threads"], dic["base_clock"], dic["turbo"], dic["memory"], scores["1"],scores["2"],scores["4"],scores["8"],scores["64"])))
                    else:
                        _sonder_log("More than 1 CPU object!", id)
                if key.startswith("gpu"):
                    number = int(key[4:])
                    dic = data[key]
                    gpu_id = int(_insert_gpu(id, dic["name"], dic["Manufacturer"], dic["Model"], dic["Board Partner"], dic["driver"], dic["clock"], dic["memory"], dic["memory-clock"], dic["Splatting"], dic["Gravity"], dic["MRender"], dic["Lighting"], dic["Parallax"], dic["Reflection"]))
                    gpu_id_start, gpu_id_end = _update_min_max_val(gpu_id, gpu_id_start, gpu_id_end)
                        #update_list.append(("gpu"+str(number), )
                if key.startswith("ram"):
                    number = int(key[4:])
                    dic = data[key]
                    if number <= 1:
                        sc = dic["Single Core"]
                        mc = dic["Multi Core"]
                        #userrun, name, product_id, form, ram_type, slots, slots_total, speed, size, latency, sc_read, sc_write, sc_mixed, sc_bandwidth, mc_read, mc_write, mc_mixed, mc_bandwidth
                        update_list.append(("ram_id", _insert_ram(id, dic["name"], dic["Manufacturer"], dic["id"], dic["form"], dic["type"], dic["slots used"], dic["slots total"], dic["speed"], dic["size"], dic["latency"],
                            sc["Read"], sc["Write"], sc["Mixed"], sc["bandwidth"], mc["Read"], mc["Write"], mc["Mixed"], mc["bandwidth"] )))    
                    else:
                        _sonder_log("More than 1 RAM object!", id)
                if key.startswith("ssd"):
                    number = int(key[4:])
                    dic = data[key]
                    sq = dic["sequential"]
                    r4 = dic["random 4k"]
                    d4 = dic["deep queue 4k"]
                    sust = dic["sustained write"]
                    smart_values = None
                    smart_id = ""
                    if "S.M.A.R.T." in dic and len(dic["S.M.A.R.T."]) > 0:
                        smart_values = _store_smart(dic["S.M.A.R.T."])
                        smart_id = smart_values[0]
                    ssd_id = _insert_ssd(id, dic["name"], dic["Manufacturer"], dic["size"], dic["firmware"], dic["free"], dic["system drive"], dic["interface"],
                        sq["Read"], sq["Write"], sq["Mixed"], sq["rate"],
                        r4["4K Read"], r4["4K Write"], r4["4K Mixed"], r4["rate"], 
                        d4["DQ Read"], d4["DQ Write"], d4["DQ Mixed"], d4["rate"], 
                        sust[0], sust[1], sust[2], sust[3], sust[4], sust[5],
                        smart_id)
                    #update_list.append(("ssd_"+str(number), ssd_id))
                    ssd_id_start, ssd_id_end = _update_min_max_val(ssd_id, ssd_id_start, ssd_id_end)
                    if smart_values is not None:
                        _update_smart(smart_values[0], smart_values[1], smart_values[2], ssd_id=ssd_id)
                if key.startswith("hdd"):
                    number = int(key[4:])
                    dic = data[key]
                    #userrun, name, firmware, free, system_drive, sequential_read, sequential_write, sequential_mixed, sequential_rate,
                    #random_4k_read, random_4k_write, random_4k_mixed, random_4k_rate,
                    #sustained_write_0, sustained_write_1, sustained_write_2, sustained_write_3, sustained_write_4, sustained_write_5
                    sq = dic["sequential"]
                    r4 = dic["random 4k"]
                    d4 = dic["deep queue 4k"]
                    sust = dic["sustained write"]
                    smart_values = None
                    smart_id = ""
                    if "S.M.A.R.T." in dic and len(dic["S.M.A.R.T."]) > 0:
                        smart_values = _store_smart(dic["S.M.A.R.T."])
                        smart_id = smart_values[0]
                    hdd_id = _insert_hdd(id, dic["name"], dic["Manufacturer"], dic["size"], dic["firmware"], dic["free"], dic["system drive"],
                        sq["Read"], sq["Write"], sq["Mixed"], sq["rate"],
                        r4["4K Read"], r4["4K Write"], r4["4K Mixed"], r4["rate"],  
                        d4["DQ Read"], d4["DQ Write"], d4["DQ Mixed"], d4["rate"],
                        sust[0], sust[1], sust[2], sust[3], sust[4], sust[5],
                        smart_id)
                    #update_list.append(("hdd_"+str(number), hdd_id))
                    hdd_id_start, hdd_id_end = _update_min_max_val(hdd_id, hdd_id_start, hdd_id_end)
                    if smart_values is not None:
                        _update_smart(smart_values[0], smart_values[1], smart_values[2], hdd_id=hdd_id)
                if key.startswith("usb"):
                    number = int(key[4:])
                    dic = data[key]
                    #userrun, name, firmware, free, system_drive, sequential_read, sequential_write, sequential_mixed, sequential_rate,
                    #random_4k_read, random_4k_write, random_4k_mixed, random_4k_rate,
                    #sustained_write_0, sustained_write_1, sustained_write_2, sustained_write_3, sustained_write_4, sustained_write_5
                    sq = dic["sequential"]
                    r4 = dic["random 4k"]
                    d4 = dic["deep queue 4k"]
                    sust = dic["sustained write"]
                    usb_id = _insert_usb(id, dic["name"], dic["Manufacturer"], dic["size"], dic["speed"], dic["free"], dic["system drive"],
                        sq["Read"], sq["Write"], sq["Mixed"], sq["rate"],
                        r4["4K Read"], r4["4K Write"], r4["4K Mixed"], r4["rate"],  
                        d4["DQ Read"], d4["DQ Write"], d4["DQ Mixed"], d4["rate"],
                        sust[0], sust[1], sust[2], sust[3], sust[4], sust[5])
                    usb_id_start, usb_id_end = _update_min_max_val(usb_id, usb_id_start, usb_id_end)
                    #update_list.append(("usb_"+str(number), usb_id))
                if key.startswith("latencies"):
                    dic = data[key]
                    update_list.append(("latencies_id", _insert_latencies(id, dic[0], dic[1], dic[2], dic[3], dic[4], dic[5], dic[6], dic[7], dic[8], dic[9], dic[10], dic[11], dic[12], dic[13], dic[14])))
            #print(json.dumps(update_list, indent=4))

            temp = _add_update_list(gpu_id_start, gpu_id_end, ssd_id_start, ssd_id_end, hdd_id_start, hdd_id_end, usb_id_start, usb_id_end)
            for t in temp:
                if temp[t] is not None:
                    update_list.append((t, temp[t]))

            _update_userrun(id, update_list)
            if ids.is_json(int(data["id"])):
                with open(os.path.join("errors", str(data["id"])+'.json'), 'w') as outfile:
                    json.dump(data, outfile)
        except:
            print("", flush=True)
            print("!Error while commiting!", flush=True)
            print("", flush=True)
            with open(os.path.join("errors", str(data["id"])+'.json'), 'w') as outfile:
                json.dump(data, outfile)
            with open(os.path.join("errors", str(data["id"])+'.database_error'), 'w') as outfile:
                outfile.write(traceback.format_exc())

def _store_smart(smart):
    if smart is None or len(smart) == 0:
        return
    low_id = None
    high_id = None
    smart_id = _insert_smart()
    for entry in smart:
        #entry["ID"] = mp[0]
        #entry["Attribute"] = mp[1].strip()
        #entry["Now"] = mp[2]
        #entry["Worst"] = mp[3]
        #entry["Value"] = mp[4]
        _store_smart_attribute(entry["ID"], entry["Attribute"])
        entry_id = _insert_smart_entries(smart_id, int(entry["ID"], 16), entry["Now"], entry["Worst"], entry["Value"])
        if low_id is None:
            low_id = entry_id
        high_id = entry_id
    return (smart_id, low_id, high_id+1)

def _store_smart_attribute(code, attribute):
    cur = thread.conn.cursor()
    cur.execute("SELECT name FROM smart_attribute WHERE code = "+str(int(code,16))+"")
    data = cur.fetchone()
    if data is None:
        _insert_into("smart_attribute", "(code, name)", (int(code,16), attribute))

def _insert_into(table, k, v):
    q = "(?" + ( ",?" * (len(v)-1)) + ")"
    #print(str(k)+"    "+str(tuple(v)))
    cur = thread.conn.cursor()
    #print("insert into "+table+" "+k+" VALUES "+q+"")
    cur.execute("insert into "+table+" "+k+" VALUES "+q+"", tuple(v))
    #print(cur.lastrowid)
    return cur.lastrowid

def _add_update_list(gpu_id_start, gpu_id_end, ssd_id_start, ssd_id_end, hdd_id_start, hdd_id_end, usb_id_start, usb_id_end):
    return locals()


def _insert_cpu(userrun, name, manufacturer, cpus, cores, threads, base_clock, turbo, memory, core_1, core_2, core_4, core_8, core_64):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("cpu", k, v)

def _insert_gpu(userrun, name, manufacturer, model, board_partner, driver, clock, memory, memory_clock, splatting, gravity, mrender, lighting, parallax, reflection):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("gpu", k, v)

def _insert_hdd(userrun, name, manufacturer, size, firmware, free, system_drive, sequential_read, sequential_write, sequential_mixed, sequential_rate,
    random_4k_read, random_4k_write, random_4k_mixed, random_4k_rate,
    deep_queue_4k_read,deep_queue_4k_write,deep_queue_4k_mixed,deep_queue_4k_rate,
    sustained_write_0, sustained_write_1, sustained_write_2, sustained_write_3, sustained_write_4, sustained_write_5, smart_id):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("hdd", k, v)

def _insert_usb(userrun, name, manufacturer, size, speed, free, system_drive, sequential_read, sequential_write, sequential_mixed, sequential_rate,
    random_4k_read, random_4k_write, random_4k_mixed, random_4k_rate,
    deep_queue_4k_read,deep_queue_4k_write,deep_queue_4k_mixed,deep_queue_4k_rate,
    sustained_write_0, sustained_write_1, sustained_write_2, sustained_write_3, sustained_write_4, sustained_write_5):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("usb", k, v)

def _insert_latencies(userrun, l0, l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("latencies", k, v)

def _insert_ram(userrun, name, manufacturer, product_id, form, ram_type, slots, slots_total, speed, size, latency, sc_read, sc_write, sc_mixed, sc_bandwidth, mc_read, mc_write, mc_mixed, mc_bandwidth):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("ram", k, v)

def _insert_ssd(userrun, name, manufacturer, size, firmware, free, system_drive, interface, sequential_read, sequential_write, sequential_mixed, sequential_rate,
    random_4k_read, random_4k_write, random_4k_mixed, random_4k_rate,
    deep_queue_4k_read,deep_queue_4k_write,deep_queue_4k_mixed,deep_queue_4k_rate,
    sustained_write_0, sustained_write_1, sustained_write_2, sustained_write_3, sustained_write_4, sustained_write_5, smart_id):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("ssd", k, v)

def _insert_userrun(id, run_date, uptime, background_cpu, os, system, motherboard, Motherboard_vendor, country, user, bios_date, memory_free, memory, display_width, display_height, display_colors, userrun_group):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("userrun", k, v)

def _insert_smart():
    cur = thread.conn.cursor()
    cur.execute("INSERT INTO smart DEFAULT VALUES")
    return cur.lastrowid

def _insert_smart_entries(smart_id, code, now, worst, value):
    (k, v) = ("("+",".join(locals().keys())+")", locals().values())
    return _insert_into("smart_entries", k, v)

def _update_table(id, values, table):
    if len(values) == 0:
        return

    q = "(?" + ( ",?" * (len(values)-1)) + ")"
    key_list = []
    value_list = []
    for (k, v) in values:
        key_list.append(k)
        value_list.append(v)

    try:
        cur = thread.conn.cursor()
        cur.execute("update "+table+" SET ("+(",".join(key_list))+") = "+q+" WHERE id = "+str(id)+"", value_list)
    except:
        print("", flush=True)
        print("!SQL ERROR!", flush=True)
        print("", flush=True)
        with open(os.path.join("errors", str(id)+'.sql_error'), 'w') as outfile:
            outfile.write(traceback.format_exc())
        with open(os.path.join("errors", str(id)+'.sql_cmd'), 'w') as outfile:
            outfile.write("update "+table+" SET ("+(",".join(key_list))+") = "+q+" WHERE id = "+str(id)+"\n"+str(value_list))

def _update_smart(id, start_id, end_id, hdd_id=None, ssd_id=None):
    if hdd_id == None and ssd_id == None:
        print("!!! ERROR !!!", flush=True)
        print("A hdd or ssd id has to be specified!", flush=True)
    values = [("start_id", start_id), ("end_id", end_id)]
    if hdd_id != None:
        values.append(("hdd_id", hdd_id))
    if ssd_id != None:
        values.append(("ssd_id", ssd_id))
    _update_table(id, values, "smart")

def _update_userrun(id, values):
    _update_table(id, values, "userrun")

    #print(print(print(print(cur.lastrowid)


#    def _insert_cpu(userrun, name, core_1, core_2, core_4, core_8, core_64):
#       print("("+",".join(locals().keys())+")")
#        print(locals().values())

#_insert_cpu(1,"8400", 1, 2, 4, 8, 64)

thread = Database()
thread.start()