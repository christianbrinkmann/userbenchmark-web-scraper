#!/bin/python
import six
import urllib
if six.PY2:
        import urllib2
        from urllib2 import Request, urlopen
elif six.PY3:
        from urllib.request import Request, urlopen
        import urllib3
        import urllib.request
        import socks
        import socket
import re

from . import header

amount = 0

def get_amount():
    global amount

    user_agent = header.get_user_agent()
    url = 'https://www.userbenchmark.com/'
    values = header.get_values()

    headers = {'User-Agent': user_agent}
    try:
        if six.PY2:
            data = urllib.urlencode(values)
            req = urllib2.Request(url, data, headers)
            response = urllib2.urlopen(req)
        elif six.PY3:
            socks.set_default_proxy(socks.SOCKS5, "localhost", 9050)
            socket.socket = socks.socksocket
            data = urllib.parse.urlencode(values).encode("utf-8")
            req = urllib.request.Request(url, data)
            #print(headers)
            for k in headers:
                    req.add_header(k, headers[k])
            response = urllib.request.urlopen(req)
        the_page = response.read()
    except:
        print("Error while receiving data for "+str(id)+"", flush=True)
        return None

    if six.PY2:
        res = the_page
    elif six.PY3:
        res = str(the_page, "UTF-8")

    amount = 0

    #class='autoCountUp'>25,645,548</span> PCs tested.</a>
    for x in res.splitlines():
        if "PCs tested" in x:
            amount = int((" ".join(re.search(r"class='autoCountUp'>(.*)</span> PCs tested", x, re.IGNORECASE).groups()) ).replace(",","") )
        if "We need to ensure that you are not a robot" in x:
            print("ROBOT CHECK! ",flush = True)
            return 0



    print("..."+str(amount)+" Benchmarks found online.", flush=True)

    return amount