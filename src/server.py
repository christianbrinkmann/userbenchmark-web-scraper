import socket, math, os, time, ssl, json, time, traceback
from socket import AF_INET, SOCK_STREAM, SO_REUSEADDR, SOL_SOCKET, SHUT_RDWR
from threading import Thread, Lock
from datetime import datetime
from socketserver import ThreadingMixIn 

from . import database, ids, country_code, server_logger
import config

ips_used = {}

class ConsoleThread(Thread):

    def __init__(self):
        Thread.__init__(self)
        print("Console thread started...", flush=True)

    def run(self):
        running = True
        cmd_in = ""
        while running:
            
            time.sleep(1)
            cmd_in = input("Command: ")

            if cmd_in == "stop":
                print("Missing command...", flush=True)
            if cmd_in[:2] == "id":
                print("Read in id...", flush=True)
                if len(cmd_in) > 3:
                    id = int(cmd_in[3:])
                    print("Request id="+str(id), flush=True)
                    ids.man_req(id)
                else:
                    print("id <id>", flush=True)
                    time.sleep(2)

# Thread the server is using to handle each client
# Multithreaded Python server : TCP Server Socket Thread Pool
class ClientHandlingThread(Thread): 
    
    conn = None
    threads = None
    threads_mutex = None
    ip = None
    port = None

    client_id = "--"
    
    def __init__(self, ip, port, conn, threads, threads_mutex): 
        Thread.__init__(self) 
        self.ip = ip 
        self.port = port 
        self.conn = conn
        self.threads = threads
        self.threads_mutex = threads_mutex
        #print("[+] New server socket thread started for " + ip + ":" + str(port))
    
    def print_log(self, text, flush=True):
        print("["+datetime.now().strftime("%H:%M:%S")+"]["+self.client_id+"]"+str(text), flush=flush)

    def run(self):
        data = b""

        self.print_log("Receive client id...", flush=True)
        start_time = time.time()
        while len(data) < 2 and time.time() - start_time < config.RECEIVE_TIMEOUT:
            data = data + self.conn.recv(2048)
        if time.time() - start_time < config.RECEIVE_TIMEOUT:
            country_code_int = int.from_bytes(data[:2], 'big')
            country_code_str = country_code.country_code_as_int_to_str(country_code_int)
            self.client_id = country_code_str
            self.print_log("Client id: "+str(self.client_id), flush=True)
            data = data[2:]

            while True:
                try:
                    start_time = time.time()
                    while len(data) < 4 and time.time() - start_time < config.RECEIVE_TIMEOUT:
                        data = data + self.conn.recv(2048)
                    if time.time() - start_time >= config.RECEIVE_TIMEOUT:
                        break
                    client_ip = int.from_bytes(data[:4], 'big')
                    data = data[4:]
                    _run_server(self.conn, client_ip, self)
                except:
                    self.print_log(traceback.format_exc(), flush=True)
                    self.print_log("Stopping thread...", flush=True)
                    break

        else:
            self.print_log("Timeout!", flush=True)
        self.conn.close()
        self.threads_mutex.acquire()
        self.threads.remove(self)
        self.threads_mutex.release()
        self.print_log("Thread closed.", flush=True)

    def _log_event(self, ip):
        server_logger.log_download(self.client_id, ip)

def run_server(threads, threads_mutex):
    if config.USE_INPUT:
        ct = ConsoleThread()
        ct.start()
    else:
        print("No console thread started", flush=True)

    TCP_IP = config.SERVER_HOST_ADDRESS
    TCP_PORT = config.SERVER_HOST_PORT

    print("Begin binding port "+str(TCP_PORT), flush=True)
    tcpServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    tcpServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
    tcpServer.bind((TCP_IP, TCP_PORT)) 
    print("port "+str(TCP_PORT)+" bound", flush=True)
    
    while True: 
        tcpServer.listen(100) 
        #print("Multithreaded Python server : Waiting for connections from TCP clients...")
        (conn, (ip,port)) = tcpServer.accept() 
        #print("Got connection from "+str(ip)+":"+str(port))
        new_thread = ClientHandlingThread(ip, port, conn, threads, threads_mutex) 
        new_thread.start()
        threads_mutex.acquire()
        threads.append(new_thread)
        threads_mutex.release()
    
    
    threads_mutex.acquire()
    for t in threads: 
        t.join()
    threads_mutex.release()

def _send_id(connection, id):
    connection.send((id).to_bytes(4, byteorder='big'))

def _get_data(connection):
    buffer = b""
    start_time = time.time()
    while len(buffer) < 4 and time.time() - start_time < config.RECEIVE_TIMEOUT:
        buffer = buffer + connection.recv(2048)
    if time.time() - start_time >= config.RECEIVE_TIMEOUT:
        print("get id timeout", flush=True)
        return None
    message_length = int.from_bytes(buffer[:4], 'big')
    if message_length == 0:
        return None
    buffer = buffer[4:]
    #print("    length received: "+str(message_length), flush=True)

    start_time = time.time()
    while len(buffer) < message_length and time.time() - start_time < config.RECEIVE_TIMEOUT:
        buffer = buffer + connection.recv(2048)
        #if len(buffer) < message_length:
            #print("    buffer length: "+str(len(buffer)), flush=True)
    if time.time() - start_time >= config.RECEIVE_TIMEOUT:
        print("get data timeout", flush=True)
        return None
    bs = buffer[:message_length]
    #print("    buffer filled", flush=True)
    return bytes.decode(bs,"utf-8")

def _run_server(connection, client_ip, object_for_logging):
    if client_ip in ips_used and time.time() - ips_used[client_ip] < config.MIN_TIME_BETWEEN_IP_USED:
        #print("IP already used in the past 30min", flush=True)
        _send_id(connection, (1 << 31) - 1) #code for ip already used
        return
    elif client_ip in ips_used:
        val = (time.time() - ips_used[client_ip])
        val = round(val / 60, 1)
        #print("IP used "+str(val)+ " Minutes ago.", flush=True)
    print(str(_int_to_ip(client_ip)), flush=True)
    ips_used[client_ip] = time.time()
    id = ids._get_id()
    #print("ID: "+str(id), flush=True)
    _send_id(connection, id)
    #print("  id sent", flush=True)
    result = _get_data(connection)
    #print("  received result", flush=True)
    if result is not None:
        data = json.loads(result)
        data["id"] = id
        sleep_timer = database.store_data(data)
        ids._add_id(id)
        if sleep_timer != 0:
            print("Sleep for "+str(sleep_timer)+"s", flush=True)
            time.sleep(sleep_timer)
        object_for_logging._log_event(client_ip)
    else:
        print("Empty result ???", flush=True)

def _int_to_ip(ipnum):
    o1 = int(ipnum / 16777216) % 256
    o2 = int(ipnum / 65536) % 256
    o3 = int(ipnum / 256) % 256
    o4 = int(ipnum) % 256
    return '%(o1)s.%(o2)s.%(o3)s.%(o4)s' % locals()