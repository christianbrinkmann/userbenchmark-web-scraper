import os, random

#
# Fake name generation
#

def _read_file(file):
    with open(file) as f:
        content = f.readlines()
        content = [x.strip() for x in content]
        return content

first_names = _read_file(os.path.join("ressources","first_names.txt"))
middle_names = _read_file(os.path.join("ressources","middle_names.txt"))
last_names = _read_file(os.path.join("ressources","last_names.txt"))

user_agents = _read_file(os.path.join("ressources","user_agents.txt"))

def _rand(a, b):
    return random.randrange(a, b)

def get_user_agent():
    return _get_random_entry(user_agents)

def _get_random_entry(list):
    return list[_rand(0,len(list))]

def get_values():
    name = "" + _get_random_entry(first_names)
    if _rand(0,10) > 7:
        name += " " + _get_random_entry(middle_names)
    name += " " + _get_random_entry(last_names)

    values = {'name' : name,
            'language' : 'en' }
    return values