#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import re, json, datetime, six, traceback, os
from time import strptime
from datetime import date
import locale


###### Utils ######

units = {"B": 1, "KB": 10**3, "MB": 10**6, "GB": 10**9, "TB": 10**12}
unit_herz = {"Hz": 1, "KHz": 10**3, "MHz": 10**6, "GHz": 10**9}

def parseRate(rate):
    return parseSize(rate.replace("/s", ""))

def parseSize(size):
    number, unit = [string.strip() for string in size.split()]
    return int(float(number)*units[unit])

def parseHerz(herz):
    number, unit = [string.strip() for string in herz.split()]
    return int(float(number)*unit_herz[unit])

def mttotime(mt):
    tmp = (str("20"+mt[2])+"-"+str(strptime(mt[0],'%b').tm_mon).zfill(2)+"-"+str(mt[1])+" "+str(mt[3])+":"+str(mt[4])+":00")
    tmp2 = "%Y-%m-%d %H:%M:%S"
    var = datetime.datetime.strptime(tmp,tmp2)
    return int(var.strftime("%s"))

def getss(orig, pattern, v):
    return re.search(pattern, orig).group(v)

def incCounter(counter, counters):
    v = 0
    if counter in counters:
        v = counters[counter]
    counters[counter] = v + 1
    return v + 1

###### Utils ######

def parse(filecontent, id):
    list1 = [ "Memory", "OS", "BIOS Date", "Uptime", "Run Date", "Run duration", "Run user"]
    list2 = [ ("cpu", "<a class=\"ucrd-img-link\" href=\"https://cpu.userbenchmark.com/", "/Rating/", 1, "cpu")]

    list2.append( ("ram", "<a class=\"ucrd-img-link\" href=\"https://ram.userbenchmark.com/", "/Rating/", 1, "ram"))
    list2.append( ("gpu", "<a class=\"ucrd-img-link\" href=\"https://gpu.userbenchmark.com/", "/Rating/", 1, "gpu"))
    list2.append( ("ssd", "<a class=\"ucrd-img-link\" href=\"https://ssd.userbenchmark.com/", "/Rating/", 1, "ssd"))
    list2.append( ("hdd", "<a class=\"ucrd-img-link\" href=\"https://hdd.userbenchmark.com/", "/Rating/", 1, "hdd"))
    list2.append( ("usb", "<a class=\"ucrd-img-link\" href=\"https://usb.userbenchmark.com/", "/Rating/", 1, "usb"))

    cpu_list = []
    gpu_list = []
    drive_list = []
    drive_parts = ["sequential", "random 4k", "deep queue 4k"]
    drive_ind = 0
    drive_list = ["Read", "Write", "Mixed","4K Read", "4K Write", "4K Mixed","DQ Read", "DQ Write", "DQ Mixed"]
    ram_list = ["Read", "Write", "Mixed"]

    cn = [1,2,4,8,64]
    cv = [0,0,0,0,0]
    index = 0
    for c in cn:
        s = str(c)+"-Core"
        cpu_list.append( (s, "%\">"+s+" ","</div>",index,"") )
        index += 1

    list2.append( ("System", "<tr><td>System</td><td>", "\s*(?:&nbsp;)*(?:<a style=.*</a>)*</td></tr>", 1, ""))
    list2.append( ("Motherboard", "<tr><td>Motherboard</td><td>", "\s*(?:&nbsp;)*(?:<a style=.*</a>)*</td></tr>", 1, ""))

    for l in list1:
        list2.append( (l, "<tr><td>"+str(l)+"</td><td>", "</td></tr>", 1, "") )

    gpu_list.append( ("Reflection", "\">Reflection ", "\">Reflection (\d+(?:\.\d*)?)</div>") )
    gpu_list.append( ("Lighting", "\">Lighting ", "\">Lighting (\d+(?:\.\d*)?)</div>") )
    gpu_list.append( ("Parallax", "\">Parallax ", "\">Parallax (\d+(?:\.\d*)?)</div>") )
    gpu_list.append( ("MRender", "\">MRender ", "\">MRender (\d+(?:\.\d*)?)</div>") )
    gpu_list.append( ("Gravity", "\">Gravity ", "\">Gravity (\d+(?:\.\d*)?)</div>") )
    gpu_list.append( ("Splatting", "\">Splatting ", "\">Splatting (\d+(?:\.\d*)?)</div>") )
    gpu_list.append( ("Model", "#Prices\">", "#Prices\">(.*?)</a></div>") )

    data = {}

    
    data["Display Width"] = ""
    data["Display Height"] = ""
    data["Display Colors"] = ""

    data["Other Runs"] = []

    linenumber = 0
    counters = {}
    mode = ""
    modeline = 0
    mixedline = 0

    memory_line = -1
    memory_line_key = ""

    blocked = True

    for x in filecontent.splitlines(True):
        if "We need to ensure that you are not a robot" in x:
            return "blocked"

    try:
        for x in filecontent.splitlines(True):
        #f = open("tmp.txt", "r")
        #for x in f:
            linenumber += 1
            if linenumber > modeline:
                mode = ""
            for l in list2:
                if l[1] in x:
                    if len(l[4]) > 0:
                        mode = l[4]
                        modeline = linenumber + 50
                        incCounter(l[4], counters)
                        data[mode+"-"+str(counters[mode])] = {}
                        data[mode+"-"+str(counters[mode])]["name"] = ""
                        data[mode+"-"+str(counters[mode])]["Manufacturer"] = ""
                        if mode == "ssd" or mode == "hdd" or mode == "usb":
                            drive_ind = 0
                            key = mode+"-"+str(counters[mode])
                            data[key]["size"] = 0
                            data[key]["sequential"] = {"Read" : "", "Write" : "", "Mixed" : "", "rate" : ""}
                            data[key]["random 4k"] = {"4K Read" : "", "4K Write" : "", "4K Mixed" : "", "rate" : ""}
                            if mode == "usb":
                                data[key]["speed"] = ""
                            else:
                                data[key]["firmware"] = ""
                            data[key]["deep queue 4k"] = {"DQ Read" : "", "DQ Write" : "", "DQ Mixed" : "", "rate" : ""}
                            if mode == "ssd":
                                data[key]["interface"] = ""
                            #if mode == "hdd":
                            data[key]["sustained write"] = ("", "", "", "", "", "")
                        if mode == "ram":
                            key = mode+"-"+str(counters[mode])
                            data[key]["Single Core"] = {}
                            data[key]["Multi Core"] = {}
                            for rw in ram_list:
                                data[key]["Single Core"][rw] = 0
                                data[key]["Multi Core"][rw] = 0
                            data[key]["Single Core"]["bandwidth"] = 0
                            data[key]["Multi Core"]["bandwidth"] = 0

                            data[key]["form"] = ""
                            data[key]["type"] = ""
                            data[key]["slots used"] = ""
                            data[key]["slots total"] = ""
                            data[key]["speed"] = ""
                            data[key]["size"] = ""
                            data[key]["latency"] = ""
                        if mode == "cpu":
                            key = mode+"-"+str(counters[mode])
                            data[key]["turbo"] = 0
                            data[key]["base_clock"] = 0
                            data[key]["memory"] = 0
                            data[key]["n-core score"] = {1: 0, 2:0, 4:0, 8:0, 64:0}
                        if mode == "gpu":
                            key = mode+"-"+str(counters[mode])
                            data[key]["Model"] = ""
                            data[key]["Board Partner"] = ""
                            data[key]["clock"] = ""
                            data[key]["memory-clock"] = ""
                            data[key]["memory"] = ""
                            data[key]["driver"] = ""
                            for g in gpu_list:
                                data[key][g[0]] = ""
                    else:
                        data[l[0]] = getss(x, l[1]+"(.*?)"+l[2], l[3])
            key = ""
            if "<tr><td>Display</td><td>" in x and " Bit" in x:
                mp = re.search(r"(\d+) x (\d+) - (\d+) Bit", x)
                if mp is not None:
                    m = mp.groups()
                    data["Display Width"] = m[0]
                    data["Display Height"] = m[1]
                    data["Display Colors"] = m[2]
                else:
                    print("Error while Display matching...", flush=True)
                    print(x, flush=True)
            if " ago" in x:
                matches = re.findall("\"https://www.userbenchmark.com/UserRun/(\d+)\"", x)
                for m in matches:
                    data["Other Runs"].append(m)
            if len(mode) > 0:
                key = mode+"-"+str(counters[mode])
                if "<div class=\"semi-strong lighterblacktext smallp\" title=\"" in x:
                    #data[key] = {}
                    data[key]["name"] = re.search(r"title=(?:.*?)>(.*?)(?:<span(?:.*)span>)?\n", x).group(1)
                    parts = data[key]["name"].split()
                    if parts is not None and len(parts) > 0:
                        data[key]["Manufacturer"] = parts[0]
                    else:
                        data[key]["Manufacturer"] = ""
                    if mode == "ssd" or mode == "hdd" or mode == "usb":
                        ms = re.findall(r"(\d+(?:,\d+)?(?:\.\d+)?)\s?([kmgtpe]b)", x, re.IGNORECASE)
                        if ms is not None and len(ms) > 0:
                            data[key]["size"] = parseSize(" ".join(ms[len(ms)-1]).replace(",",""))
                    #if mode == "ssd":
                    #    print(key+": "+data[key]["name"])
            if mode == "gpu":
                if "CLim:" in x:
                    data[key]["clock"] = parseHerz(re.search(r"CLim: (\d* [A-Z]?Hz)", x,re.IGNORECASE).group(1))
                if "MLim:" in x:
                    data[key]["memory-clock"] = parseHerz(re.search(r"MLim: (\d* [A-Z]?Hz)", x,re.IGNORECASE).group(1))
                if "Ram:" in x:
                    data[key]["memory"] = parseSize((" ".join(re.search(r"Ram: (\d+(?:\,\d+)?(?:\.\d+)?)\s*([kmgtp]?b)", x,re.IGNORECASE).groups())).replace(",",""))
                if "Driver:" in x:
                    data[key]["driver"] = re.search(r"Driver: (.*)</div>", x,re.IGNORECASE).group(1)
            #if "<div>CLim:" in x:
                    #<div>Driver: aticfx64.dll Ver. 14.301.0.0</div>
                    #<div>CLim: 1961 MHz, MLim: 1752 MHz, Ram: 4GB, Driver: 431.70</div>
                    #matches = re.search(r"<div>CLim: (\d* [A-Z]?Hz), MLim: (\d* [A-Z]?Hz), Ram: (\d+(?:\.\d+)?)\s*([kmgtp]?b), Driver: (.*)</div>", x,re.IGNORECASE).groups()
                    #data[key]["clock"] = parseHerz(matches[0])
                    #data[key]["memory-clock"] = parseHerz(matches[1])
                    #data[key]["memory"] = parseSize(matches[2]+" "+matches[3])
                    #data[key]["driver"] = matches[4]
                for l in gpu_list:
                    if l[1] in x:
                        data[key][l[0]] = re.search(l[2], x, re.IGNORECASE).group(1)
                        if l[0] == "Model":
                            parts = data[key][l[0]].split("(")
                            if parts is not None and len(parts) > 0:
                                data[key]["Board Partner"] = parts[0]
            if mode == "cpu":
                for l in cpu_list:
                    if l[1] in x:
                        cv[l[3]] = getss(x, l[1]+"(.*?)"+l[2], 1)
                        if l[3] == 4:
                            ind = 0
                            for c in cv:
                                data[key]["n-core score"][cn[ind]] = c.replace(",","")
                                ind += 1
                if "<div>Base clock" in x:
                    data[key]["base_clock"] = parseHerz(" ".join(re.search(r"Base clock (\d+(?:\.\d+)?)\s*([kmgtp]?hz)", x, re.IGNORECASE).groups()))
                    if "turbo" in x:
                        m = re.search(r", turbo (\d+(?:\.\d+)?)\s*([kmgtp]?hz)", x, re.IGNORECASE)
                        if m is not None:
                            data[key]["turbo"] = parseHerz(" ".join(m.groups()))
                if "CPU" in x and "core" in x and "thread" in x:
                    #<div class="gap-m-t"><span>N/A, 1 CPU, 2 cores, 4 threads</span></div>
                    matches = re.search(r"(\d+) CPU, (\d+) cores, (\d+) threads", x)
                    data[key]["cpus"] = 0
                    data[key]["cores"] = 0
                    data[key]["threads"] = 0
                    if matches is not None:
                        mp = matches.groups()
                        data[key]["cpus"] = mp[0]
                        data[key]["cores"] = mp[1]
                        data[key]["threads"] = mp[2]
                    else:
                        print("Missing matches:", flush=True)
                        print(x, flush=True)
                if "%\">Memory " in x:
                    data[key]["memory"] = re.search(r"Memory (\d+(?:\.\d+)?)", x).group(1)
            if mode == "ssd" or mode == "hdd" or mode == "usb":
                if "S.M.A.R.T." in data[key]:
                    matches = re.search(r"\(([0-9a-fA-F]{2})\)\s(.*)\s+(\d+)\s+(\d+)\s+(\d+)", x)
                    if matches is not None:
                        mp = matches.groups()
                        entry = {}
                        entry["ID"] = mp[0]
                        entry["Attribute"] = mp[1].strip()
                        entry["Now"] = mp[2]
                        entry["Worst"] = mp[3]
                        entry["Value"] = mp[4]
                        data[key]["S.M.A.R.T."].append(entry)
                if "(ID)" in x and "Attribute" in x and "Now" in x and "Worst" in x and "Value" in x:
                    data[key]["S.M.A.R.T."] = []
                #(ID) Attribute                             Now Worst Value
                #(01) Raw read error rate                   200  199  1882
                #(03) Spinup time                           135  131  4216
                #(04) Start/Stop count                       95   95  5441
                #(05) Reallocated sector count              200  200  0
                #(07) Seek error rate                       100  253  0
                #(09) Power-on hours count                   70   70  22434
                #(0A) Spinup retry count                    100  100  0
                #(0B) Calibration retry count               100  100  0
                #(0C) Power cycle count                      95   95  5138
                #(C0) Power-off retract count               199  199  936
                #(C1) Load/Unload cycle count               199  199  5440
                #(C2) HDA temperature                       104   87  39
                #(C4) Reallocation count                    200  200  0
                #(C5) Current pending sector count          200  200  0
                #(C6) Offline scan uncorrectable count      200  200  1
                #(C7) UDMA CRC error rate                   200  200  97
                #(C8) Write error rate                      200  200  1
                if "<div class=\"gap-m-t\"><span>" in x:
                    data[key]["free"] = parseSize(" ".join(re.search(r"<div class=\"gap-m-t\"><span>(\d+(?:\.\d+)?)\s*([kmgtp]?b) free", x, re.IGNORECASE).groups()))
                    data[key]["system drive"] = "(System drive)" in x
                if "Operating at" in x and "Speed" in x:
                    #Operating at USB 2.0 Speed
                    data[key]["speed"] = re.search(r"Operating at (.*) Speed", x).group(1)
                if "<div>Firmware: " in x:
                    data[key]["firmware"] = re.search(r"<div>Firmware: (.*?) (?:Max speed: (?:.*))?</div>", x).group(1)
                    if "Max speed" in x:
                        data[key]["interface"] = re.search(r"Max speed: (.*?)</div>", x).group(1)
                if "<div>SusWrite @10s intervals: " in x:
                    #data[key]["sustained write"] = re.search("<div>SusWrite @10s intervals: (\d+(?:\,\d+)?(?:\.\d+)?) (\d+(?:\,\d+)?(?:\.\d+)?) (\d+(?:\,\d+)?(?:\.\d+)?) (\d+(?:\,\d+)?(?:\.\d+)?) (\d+(?:\,\d+)?(?:\.\d+)?) (\d+(?:\,\d+)?(?:\.\d+)?) MB/s", x).groups()
                    m = re.findall("(?:(\d+(?:\,\d+)?(?:\.\d+)?)\s)", x)
                    while len(m) < 6:
                        m.append("")
                    data[key]["sustained write"] = m[:6]
                for l in drive_list:
                    if "<div class=" in x and "\">"+l in x:
                        pat = "<div class=\"\" title=\"(?:.*?)\">\s?"+l+" (\d+(?:\,\d+)?(?:\.\d+)?)</div>"
                        data[key][drive_parts[drive_ind//3]][l] = re.search(pat, x).group(1).replace(",","")
                        drive_ind += 1
                        mixedline = linenumber + 2
                if linenumber == mixedline - 1 and ">SusWrite" in x:
                    data[key][drive_parts[(drive_ind-1)//3]]["sus_write"] = re.search("<div class=\"\" title=\"(?:.*?)\">\s?SusWrite (\d+(?:\,\d+)?(?:\.\d+)?)</div>", x).group(1).replace(",","")
                    mixedline = linenumber + 2
                if linenumber == mixedline and "uc-rag sp" in x:
                    data[key][drive_parts[(drive_ind-1)//3]]["rate"] = parseRate( (" ".join(re.search(r"</div>(?:\d+(?:\,\d+)?)% (\d+(?:\,\d+)?(?:\.\d+)?)<span class=\"tc-units\"> ([tgmk]?b/s)", x, re.IGNORECASE).groups()) ).replace(",","") )
            if mode == "ram":
                if "slots used" in x and "<span>" in x:
                    matches = re.search(r"<span>(\d+) of (\d+) slots used", x).groups()
                    data[key]["slots used"] = matches[0]
                    data[key]["slots total"] = matches[1]
                if "DIMM" in x and "<div>" in x:
                    mp = re.search(r"<div>(\d+(?:\.\d+)?)([tgmk]?b) ([a-z]*dimm)", x, re.IGNORECASE)
                    if mp is not None:
                        matches = mp.groups()
                        data[key]["size"] = parseSize(matches[0]+" "+matches[1])
                        data[key]["form"] = matches[2]
                        if "clocked @" in x and "z</div>" in x:
                            data[key]["speed"] = parseHerz(re.search(r"clocked @ (\d+ [tgmk]?hz)</div>", x, re.IGNORECASE).group(1))
                        if "DDR" in x:
                            m = re.search(r" (DDR\d*)", x, re.IGNORECASE)
                            if m is not None:
                                data[key]["type"] = m.group(1)
                            else:
                                data[key]["type"] = "undefined"
                if "<div class=\"semi-strong lighterblacktext smallp\"" in x:
                    if "Unknown" in x and "title=" not in x:
                        data[key]["id"] = re.search(r"semi-strong lighterblacktext smallp\">(.*?)(\r\n?|\n)?",x).group(1)
                        data[key]["name"] = data[key]["id"]
                        data[key]["slots used"] = "0"
                        data[key]["slots total"] = "0"
                        
                        data[key]["size"] = 0
                        data[key]["form"] = ""
                        
                        data[key]["speed"] = 0
                        data[key]["type"] = ""
                    
                    else:
                        data[key]["id"] = re.search(r" title=\"(.*?)\">",x).group(1)
                for cc in ["SC", "MC"]:
                    for rw in ram_list:
                        if "%\">"+cc+" "+rw+" " in x:
                            key2 = "Single Core"
                            if cc == "MC":
                                key2 = "Multi Core"
                            if rw == "Mixed":
                                memory_line = linenumber + 2
                                memory_line_key = key2
                            data[key][key2][rw] = re.search(r"%\">"+cc+" "+rw+" (\d+(?:\,\d+)?(?:\.\d+)?)</div>", x).group(1).replace(",","")
                if linenumber == memory_line and "<span class=\"tc-units\">" in x:
                    data[key][memory_line_key]["bandwidth"] = parseRate(" ".join(re.search(r">\d+(?:\,\d+)?% (\d+(?:\.\d+)?)<span class=\"tc-units\"> ([tgmk]b/s)</span>", x, re.IGNORECASE).groups()))
                if "%\">Latency " in x:
                    data[key]["latency"] = re.search(r"\">Latency (\d+(?:\,\d+)?(?:\.\d+)?)</div>", x).group(1).replace(",","")
            if "Background CPU</td><td>" in x:
                data["Background CPU"] = getss(x, "(\d+)%", 1)
            if "data: [" in x:
                data["latencies"] = x.strip().replace("data: [", "").replace("]","").split(",")
            # <tr><td>Run User</td><td><i class="flag flag-ro"></i> ROU-User</td></tr>
            # <tr><td>Run User</td><td><i class="flag flag-us"></i> daaznguy</td></tr>
            if "<tr><td>Run User</td><td>" in x:
                data["Country"] = getss(x, "flag flag-([a-z]*)\">", 1)
                data["User"] = getss(x, "</i> (.*)</td></tr>", 1)
                #if "USBFlashPro" in x:
                #    data["Country"] = "USBFlashPro"
                #else:
                #    data["Country"] = getss(x, "></i> ([A-Z]*)-User</td>", 1)
        if "Run Date" in data:
            data["Run Date"] = mttotime(re.search(r'([A-z]{3}) (\d{2}) \'(\d{2}) at (\d{2}):(\d{2})', data["Run Date"]).groups())
        else:
            data["Run Date"] = 0
        if "Uptime" in data:
            data["Uptime"] = data["Uptime"].replace(" Days", "")
        else:
            data["Uptime"] = 0
        if "BIOS Date" in data and len(data["BIOS Date"]) > 0:
            data["BIOS Date"] = int(datetime.datetime.strptime( data["BIOS Date"][0:4]+"-"+data["BIOS Date"][4:6]+"-"+data["BIOS Date"][6:8], "%Y-%m-%d").strftime("%s"))
        else:
            data["BIOS Date"] = 0
        if "Memory" in data:
            data["Memory free"] = parseSize(" ".join(re.search(r"(\d+(?:\.\d+)?)\s*([kmgtp]?b) free of", data["Memory"], re.IGNORECASE).groups()))
            data["Memory"] = parseSize(" ".join(re.search(r"free of (\d+(?:\.\d+)?)\s*([kmgtp]?b)", data["Memory"], re.IGNORECASE).groups()))
        else:
            data["Memory"] = 0
            data["Memory free"] = 0

        if "System" not in data:
            data["System"] = "undefined"
        if "Motherboard" not in data:
            data["Motherboard"] = "undefined"
            data["Motherboard Vendor"] = "undefined"
        else:
            parts = data["Motherboard"].split()
            if parts is not None and len(parts) > 0:
                data["Motherboard Vendor"] = parts[0]
            else:
                data["Motherboard Vendor"] = ""

        if "Background CPU" not in data:
            print("", flush=True)
            print("!Error!Nothing parsed!", flush=True)
            print("", flush=True)
            with open(os.path.join("errors", str(id)+'.txt'), 'w') as outfile:
                outfile.write(filecontent)
            with open(os.path.join("errors", str(id)+'_splitlines.txt'), 'w') as outfile:
                outfile.writelines(filecontent.splitlines(True))
            with open(os.path.join("errors", str(id)+'.json'), 'w') as outfile:
                json.dump(data, outfile)
            return None

        return data
    except:
        print("", flush=True)
        print("!Error while parsing!", flush=True)
        print("", flush=True)
        with open(os.path.join("errors", str(id)+'.stringparser_error'), 'w') as outfile:
            outfile.write(traceback.format_exc())
        with open(os.path.join("errors", str(id)+'.json'), 'w') as outfile:
            json.dump(data, outfile)
        with open(os.path.join("errors", str(id)+'.txt'), 'w') as outfile:
            outfile.write(filecontent)
        

        return None
    print("Unexpected return...", flush=True)
    return None