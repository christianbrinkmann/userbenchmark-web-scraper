import os

country_code = ""
country_code_as_int = 0

def load_country_code_from_file():
    if not os.path.exists('country.txt'):
        print("country.txt not found!", flush=True)
        exit()
    
    global country_code, country_code_as_int
    
    file1 = open('country.txt', 'r')
    country_code = file1.readline()[:2]
    file1.close()
    country_code = country_code + ("a"*max(0,2-len(country_code)))

    index = 0
    for character in country_code:
        country_code_as_int += (1+ord('z')-ord('a'))**index * (ord(character)-ord('a'))
        index += 1

    print("Country-Code: "+str(country_code))
    print("Country-Code Int: "+str(country_code_as_int))

def country_code_as_int_to_str(cc_int):
    text = ""
    index = 0
    while 26**index <= cc_int:
        g = (cc_int // (26**index)) % 26
        index += 1
        text += chr(ord('a') + g)
    text = text + 'a'*(max(0,2-len(text)))
    return text

def get_country_code(ip):
    import geoip2.database
    if os.path.exists('/GeoIP/GeoLite2-City.mmdb'):
        with geoip2.database.Reader('/GeoIP/GeoLite2-City.mmdb') as reader:
            response = reader.city(ip)
            return response.country.iso_code
    return "zz"

def get_country_codes(ips):
    import geoip2.database
    countries = {}
    if os.path.exists('/GeoIP/GeoLite2-City.mmdb'):
        with geoip2.database.Reader('/GeoIP/GeoLite2-City.mmdb') as reader:
            for ip in ips:
                response = reader.city(ip)
                code = response.country.iso_code
                if code not in countries:
                    countries[code] = 0
                countries[code] += 1
    return countries