import socket, math, os, time, ssl, random, requests, traceback, re
from socket import AF_INET, SOCK_STREAM, SO_REUSEADDR, SOL_SOCKET, SHUT_RDWR
from threading import Thread 
from socketserver import ThreadingMixIn 
import json

from . import downloader, stringparser, country_code, proxy_list
import config

ip_sites = list(("https://ifconfig.me/ip", "https://ipecho.net/plain", "https://ipinfo.io/ip", "https://v4.ident.me"))


proxy_addr = None

class ClientHandle(Thread):
    
    thread = None
    conn = None
    _run = True
    keep_running = True

    def __init__(self, ip, port, conn): 
        Thread.__init__(self) 
        self.ip = ip 
        self.port = port 
        self.conn = conn
        print("[+] New client socket thread started for " + ip + ":" + str(port), flush=True)
 
    def run(self):
        global proxy_addr
        _send_client_id(self.conn)
        print("Sent country code...", flush=True)
        if config.PROXY_LIST_FILE is not None:
            proxy_index = 0
            print("Scrambling proxy list...", flush=True)
            proxy_list.get_random_list()
            proxy_addr = proxy_list.proxy_list[proxy_index]
            print("First proxy address: "+proxy_addr, flush=True)
        while self._run:
            res = _run_client(self.conn)
            if res is None:
                print("Stopping...", flush=True)
                self._run = False
                break
            if config.PROXY_LIST_FILE is not None:
                if proxy_index >= len(proxy_list.proxy_list):
                    proxy_index = 0
                    proxy_list.get_random_list()
                print("New proxy address: "+proxy_addr, flush=True)
                proxy_addr = proxy_list.proxy_list[proxy_index]
                proxy_index += 1
            else:
                os.system("killall -HUP tor")
        self.conn.close()
        print("connection closed...", flush=True)

def run_client(hostname, port):

    #reset default socket to remove default proxy
    socket.socket = downloader.original_socket

    tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    tcp_client.connect((hostname, port))

    new_thread = ClientHandle(hostname, port, tcp_client)
    new_thread.start()
    return new_thread

def _run_client(connection):
    ip = _get_ip()
    if ip is None:
        return False
    id = _get_job_id(connection, ip)
    if id == 0:
        print("No job id...", flush=True)
        return False
    if id == (1 << 31) - 1:
        print("Ip not suited. Getting a new one...", flush=True)
        return False
    if id == (1 << 31) - 2: #stop connection, reconnect anew
        print("Stop connection", flush=True)
        return None
    print("Job id: "+str(id), flush=True)
    content = downloader.downloadFile(id, proxy_addr)
    print("Downloaded for id "+str(id), flush=True)
    if content is not None:
        data = stringparser.parse(content, id)
        if data is None or "blocked" in data:
            print("   Could not read data...", flush=True)
            _send_error(connection)
        else:
            print("  sending data ("+str(len(data))+")", flush=True)
            data_size = _send_data(connection, data)
            print("  data sent: "+str(data_size), flush=True)
    else:
        print("send error", flush=True)
        _send_error(connection)
    return True

def _get_ip():
    try:
        proxies = {'http': "socks5://127.0.0.1:9050", 'https': "socks5://127.0.0.1:9050"}
        global proxy_addr
        if proxy_addr is not None:
            proxies = {'http': proxy_addr, 'https': proxy_addr}
            print(proxies, flush=True)
        res = None
        index = 0
        while res is None:
            r = requests.get(ip_sites[index % len(ip_sites)], proxies=proxies, timeout=15)
            p = r.text
            if re.search(r"(\d+)\.(\d+)\.(\d+)\.(\d+)", p) is not None:
                res = p
            index += 1
            if index % len(ip_sites) == 0:
                print("No site appears to be working. Check your internet connection. Waiting for 5s...", flush=True)
                time.sleep(5)
                print("Continue...", flush=True)
        return res
    except:
        print(traceback.format_exc())
        return None

def _get_job_id(connection, ip):
    try:
        ip_bytes = bytes(map(int, ip.split('.')))
        print("IP: " + (_ips(ip_bytes)), flush=True)
        connection.send(ip_bytes)

        data = b""
        start_time = time.time()
        while len(data) < 4 and time.time() - start_time < config.RECEIVE_TIMEOUT:
            data = data + connection.recv(2048)
        if time.time() - start_time >= config.RECEIVE_TIMEOUT:
            print("Job id receive timeout...", flush=True)
            return (1 << 31) - 2
        id = int.from_bytes(data[:4], 'big')
        data = data[4:]
        return id
    except:
        print(traceback.format_exc())
        return (1 << 31) - 2

def _send_client_id(connection):
    print(country_code.country_code_as_int.to_bytes(2, byteorder='big'))
    connection.send(country_code.country_code_as_int.to_bytes(2, byteorder='big'))

def _send_data(connection, data):
    raw = str.encode(json.dumps(data),"utf-8")
    message_length = len(raw)
    size_prefix = (message_length).to_bytes(4, byteorder='big')
    connection.send(size_prefix)
    connection.send(raw)
    return size_prefix

def _send_error(connection):
    size_prefix = (0).to_bytes(4, byteorder='big')
    connection.send(size_prefix)

def _ips(ips):
    return str(int(ips[0]))+"."+str(int(ips[1]))+"."+str(int(ips[2]))+"."+str(int(ips[3]))

def _int_to_ip(ipnum):
    o1 = int(ipnum / 16777216) % 256
    o2 = int(ipnum / 65536) % 256
    o3 = int(ipnum / 256) % 256
    o4 = int(ipnum) % 256
    return '%(o1)s.%(o2)s.%(o3)s.%(o4)s' % locals()