import six
import urllib
if six.PY2:
        import urllib2
        from urllib2 import Request, urlopen
        import socks
        import socket
elif six.PY3:
        from urllib.request import Request, urlopen
        import urllib3
        import urllib.request
        import socks
        import socket

from . import header

original_socket = socket.socket

def _fake_dl():
    data = None
    with open('../19013687.txt', 'r') as file:
        data = file.read()
    return data

def downloadFile(id, proxy_addr):
    #return _fake_dl()
    user_agent = header.get_user_agent()
    url = 'https://www.userbenchmark.com/UserRun/'+str(id)
    values = header.get_values()

    headers = {'User-Agent': user_agent}
    response = None
    try:
        if six.PY2:
            socks.set_default_proxy(socks.SOCKS5, "localhost", 9050)
            socket.socket = socks.socksocket
            data = urllib.urlencode(values)
            req = urllib2.Request(url, data, headers)
            response = urllib2.urlopen(req)
        elif six.PY3:
            if proxy_addr is not None:
                #"socks5://127.0.0.1:9050"
                socks_prefix, hostname, port = proxy_addr.replace("//","").split(":")
                port = int(port)
                if socks_prefix == "socks5":
                    socks.set_default_proxy(socks.SOCKS5, hostname, port)
                elif socks_prefix == "socks4":
                    socks.set_default_proxy(socks.SOCKS4, hostname, port)
            else:
                socks.set_default_proxy(socks.SOCKS5, "localhost", 9050)
            socket.socket = socks.socksocket
            data = urllib.parse.urlencode(values).encode("utf-8")
            req = urllib.request.Request(url, data)
            #print(headers)
            for k in headers:
                    req.add_header(k, headers[k])
            response = urllib.request.urlopen(req)
        the_page = response.read()
    except:
        print("Error while receiving data for "+str(id)+"", flush=True)
        return None

    if six.PY2:
        res = the_page
    elif six.PY3:
        res = str(the_page, "UTF-8")

    #f = open("tmp.txt","w+", encoding="utf-8")
    #f.write(res)
    #f.close()

    return res