import random

proxy_list = []

def load_file(file_path):
    content = ""
    with open(file_path, 'r') as file:
        content = file.read()
    for line in content.split():
        proxy_list.append(line)
    print(str(len(proxy_list))+" Proxies found.")

def get_random_list():
    random.shuffle(proxy_list)
    return proxy_list