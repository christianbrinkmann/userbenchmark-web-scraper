import random
from threading import Thread, Lock

ids = None
id_amount = 0
group_id_max = -1
id_mutex = Lock()
manual_requests = []
jsons = set()

def init_ids(amount):
    global ids
    print("Initializing ids", flush=True)
    r = amount // 8
    ids = bytearray(r)

    j=0
    while j < 2519:
        _add_id(j)
        j += 1
    global id_amount
    id_amount = 0
    
    return r

def man_req(id):
    id_mutex.acquire()
    manual_requests.append(id)
    jsons.add(id)
    id_mutex.release()

def is_json(id):
    id_mutex.acquire()
    result = (id in jsons)
    id_mutex.release()
    return result

def _add_id(id):
    global ids
    id_mutex.acquire()
    ids[id//8] |= (1 << (id % 8))
    global id_amount
    id_amount += 1
    id_mutex.release()

def _is_not_missing(id):
    global ids
    if (ids[id//8] & (1 << (id % 8))) != 0:
        return True
    return False

def _get_id():
    global ids
    id_mutex.acquire()
    id = 0
    iters = 0
    if len(manual_requests) > 0:
        id = manual_requests.pop()
        id_mutex.release()
        return id
    while _is_not_missing(id) and iters < 1000:
        id = random.randint(1, len(ids)*8)
        iters += 1
    if iters >= 1000:
        i = 1
        while _is_not_missing(i):
            i += 1
        id = i
    id_mutex.release()
    return id